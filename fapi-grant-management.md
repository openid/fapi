# Grant Management for OAuth 2.0

The Grant Management for OAuth 2.0 was previously known as the FAPI Grant Management. 

The latest HTML version of the document can be found here: https://openid.bitbucket.io/fapi/oauth-v2-grant-management.html .

The source on bitbucket can be found here: https://bitbucket.org/openid/fapi/src/master/oauth-v2-grant-management.md .