# JWT Secured Authorization Response Mode for OAuth 2.0 (JARM)

This location previously contained drafts of the JWT Secured Authorization Response Mode for OAuth 2.0 (JARM).

The file can now be found here: https://bitbucket.org/openid/fapi/src/master/oauth-v2-jarm.md
